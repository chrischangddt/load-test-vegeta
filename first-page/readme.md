vegeta attack -targets ./first-page.txt -duration 1s -rate 1 -timeout 30s -workers 10 | vegeta report -every 1s

vegeta attack -targets ./first-page.txt -duration 5s -rate 1 -timeout 30s -workers 10 | vegeta encode --to csv --output result.csv
