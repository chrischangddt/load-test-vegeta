# Load Testing Using Vegeta
integration with vegeta attack

## UserBinding Attack

### how to attack

```bash
# using vegeta attack by dynamic payload
# hit rate:    300 hits/sec
# report rate: 1   refresh/sec

python attack.py --route datamapping/UserBinding --user_src_file ./dummy/user.csv | \
vegeta attack -lazy -format=json -rate 3 -duration 0s -timeout 30s -workers 10 | \
vegeta report -every 1s
```

```bash
python run.py | vegeta attack -lazy -format=json -rate 3 -duration 0s -timeout 30s -workers 10 | vegeta report -every 1s

python run.py | \
vegeta attack -lazy -format=json -rate 300 -duration 0s -timeout 30s -workers 10 | \
vegeta encode | \
jaggr @count=rps \
          hist\[100,200,300,400,500\]:code \
          p25,p50,p95:latency \
          sum:bytes_in \
          sum:bytes_out | \
jplot rps+code.hist.100+code.hist.200+code.hist.300+code.hist.400+code.hist.500 \
        latency.p95+latency.p50+latency.p25 \
        bytes_in.sum+bytes_out.sum
```