"""
Usage:  attack.py --route=<route> --user_src_file=<user_src_file>
        attack.py -h | --help

Options:
    --route=route                       Route for api
    --user_src_file=user_src_file       Path for users csv file
"""

import os
import docopt

from apihub import apihub
from dotenv import load_dotenv


if __name__ == '__main__':
    result = load_dotenv(
        dotenv_path=f'{os.getcwd()}/.env'
    )

    import settings
    args = docopt.docopt(__doc__)

    route = args['--route']
    user_src_file = args['--user_src_file']

    apihub[route](settings, user_src_file)
