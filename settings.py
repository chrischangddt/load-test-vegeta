import os

SERVICE_ENDPOINT = os.environ.get('SERVICE_ENDPOINT')

PROJECT_ID = os.environ.get('PROJECT_ID')

IDENTITY_POOL_ID = os.environ.get('IDENTITY_POOL_ID')

CLIENT_ID = os.environ.get('CLIENT_ID')

PROJECT_API_KEY = os.environ.get('PROJECT_API_KEY')
