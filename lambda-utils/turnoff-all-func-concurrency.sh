for n in $(cat ./common-functions-provisioned-list.txt)
do
  function_name=$(echo $n | cut -d "|" -f 1)

  aws lambda delete-provisioned-concurrency-config \
    --function-name $function_name \
    --qualifier provisioned
done
