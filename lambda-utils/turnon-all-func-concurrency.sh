for n in $(cat ./common-functions-provisioned-list.txt)
do
  function_name=$(echo $n | cut -d "|" -f 1)

  aws lambda put-provisioned-concurrency-config \
    --function-name $function_name \
    --qualifier provisioned \
    --provisioned-concurrent-executions 1
done