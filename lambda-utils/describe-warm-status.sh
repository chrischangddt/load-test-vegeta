for n in $(cat ./warm-functions-config.txt)
do
  function_name=$(echo $n | cut -d "|" -f 1)
  provisioned_amount=$(echo $n | cut -d "|" -f 2)

  aws lambda list-provisioned-concurrency-configs \
    --function-name $function_name
done
