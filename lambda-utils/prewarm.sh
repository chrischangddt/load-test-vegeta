for n in $(cat ./warm-functions-config.txt)
do
  function_name=$(echo $n | cut -d "|" -f 1)
  provisioned_amount=$(echo $n | cut -d "|" -f 2)

  aws lambda put-provisioned-concurrency-config \
    --function-name $function_name \
    --qualifier provisioned \
    --provisioned-concurrent-executions $provisioned_amount
done