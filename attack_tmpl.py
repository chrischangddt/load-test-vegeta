import secrets
import random
import json
import base64
import pandas as pd

user_amount_limit = 10


def make_body_payload(project_id, identity_pool_id, identity_id):
    return {
        'Context': {
            'ProjectId': project_id,
            'IdentityPoolId': identity_pool_id,
            'IdentityId': identity_id
        }
    }


def make_request(endpoint, api_key, payload, serialize=True):
    encode_body = base64.b64encode(bytes(json.dumps(payload), encoding='utf-8')).decode('utf-8')
    request = {
        'method': 'POST',
        'url': endpoint,
        'body': encode_body,
        'header': {
            'Authorization': [f'ApiKey {api_key}']
        }
    }

    if serialize:
        return json.dumps(request)

    return json.dumps(request)


def attack(settings, user_src_file):
    df = pd.read_csv(user_src_file, usecols=[1], names=['identity_id'])
    df = df[0:user_amount_limit]

    project_id = settings.PROJECT_ID
    identity_pool_id = settings.IDENTITY_POOL_ID
    client_app_id = settings.CLIENT_ID

    project_api_key = settings.PROJECT_API_KEY
    endpoint = settings.SERVICE_ENDPOINT

    while True:
        select_user_idx = random.randint(0, user_amount_limit - 1)

        identity_id = df['identity_id'][select_user_idx]

        payload = make_body_payload(project_id, identity_pool_id, identity_id)
        request = make_request(f'{endpoint}//', project_api_key, payload)

        print(request)
